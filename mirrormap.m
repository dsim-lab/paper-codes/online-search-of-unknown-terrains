function [Xcoord, Ycoord] = mirrormap(Xcoord,Ycoord,xmin,xmax,ymin,ymax)
% MIRRORMAP Transforms points that are lying outside the maps boundries.
% 
% Input:
%     xmin   = left hand boundary of the map
%     xmax   = right hand boundary of the map
%     ymin   = lower boundary of the map
%     ymax   = upper boundary of the map
%     Xcoord = robot's X position
%     Ycoord = robot's Y position
% 
% Output:
%     Xcoord = robot X position
%     Ycoord = robot Y position
% 
%     
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edit:
%     09/14/2021 -- Reformatted code and comments (Patrick McNamee)
%     
    
%factor to offset the points away from the edge of the boundary. With this
%factor, the robot can stay significantly away from the boundary
fac = 0.5; 

if Xcoord < xmin + fac
    Xcoord = -Xcoord + 2*(xmin + fac);
elseif Xcoord > xmax - fac
    Xcoord = -Xcoord + 2*(xmax - fac);
end

if Ycoord < ymin + fac
    Ycoord = -Ycoord + 2*(ymin + fac);
elseif Ycoord > ymax - fac
    Ycoord = -Ycoord + 2*(ymax - fac);
end

end
