function [Xcoord, Ycoord] = ...
    mirrormap_obs(Xcoord,Ycoord,obs_vec,xmin,xmax,ymin,ymax,fac)
% MIRROR_OBS Transforms points that are lying inside the obstacles
% 
% Input:
%     obs_vec                = vector consisting of obstacle coordinates
%     Xcoord                 = robot X position
%     Ycoord                 = robot Y position
%     xmin, xmax, ymin, ymax = obstacle boundaries
%     fac                    = tolarence for obstacle
% 
% Output:
%     Xcoord = robot X position after potential mapping
%     Ycoord = robot Y position after potential mapping
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: 09/14/2021
% 
% Edit:
%     09/14/2021 -- Reformatted code and comments (Patrick McNamee)
%     09/14/2021 -- Fixed incorrect logic on obstacle check (Patrick McNamee)
%

% factor to offset the points away from the edge of the obstacle. With this
% factor, the robot can stay significantly away from the obstacle

if isempty(fac)
    fac = 0.1;
end

%checks for each obstacle
for i = 1:2:length(obs_vec)
    xmin_obs = obs_vec(1,i);   xmax_obs = obs_vec(2,i);
    ymin_obs = obs_vec(1,i+1); ymax_obs = obs_vec(2,i+1);
    
    % If inside obstacle or simply too close
    if (Xcoord > xmin_obs - fac) && (Xcoord < xmax_obs + fac) && ...
       (Ycoord > ymin_obs - fac) && (Ycoord < ymax_obs + fac)
        
        % determine the minimum distance from a reflection axis without
        % checking the map.
        distances = [...
            Xcoord - xmin_obs + fac;...
           -Xcoord + xmax_obs + fac;...
            Ycoord - ymin_obs + fac;...
           -Ycoord + ymax_obs + fac;...
        ];  % all should be positive based on if conditional statement
   
        % Edit reflections outside the map to be infinite distance
        if xmin_obs - fac < xmin
            distances(1) = inf;
        end
        if xmax_obs + fac > xmax
            distances(2) = inf;
        end
        if ymin_obs - fac < ymin
            distances(3) = inf;
        end
        if ymax_obs + fac > ymax
            distances(4) = inf;
        end

        if sum(distances < fac) == 2
            % In a corner, needs both an x and y reflection
            % Reflection for X coordinate
            if distances(1) < distances(2)
                Xcoord = xmin_obs - fac;
            else
                Xcoord = xmax_obs + fac;
            end

            % Reflection for Y coordinate
            if distances(3) < distances(4)
                Ycoord = ymin_obs - fac;
            else
                Ycoord = ymax_obs + fac;
            end
        else
            % Find minimum argument, assuming at least 1 finite value. If
            % there are no finite values, then there is no valid reflection
            % possible.
            [~, min_dist_ind] = min(distances);
        
            % Reflect across the closest axis of reflection for mirror
            % mapping.
            switch min_dist_ind
                case 1
                    Xcoord = xmin_obs - fac;
                case 2
                    Xcoord = xmax_obs + fac;
                case 3
                    Ycoord = ymin_obs - fac;
                case 4
                    Ycoord = ymax_obs + fac;
            end
        end
    end
end
