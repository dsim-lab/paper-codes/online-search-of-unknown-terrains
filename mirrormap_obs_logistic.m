function [X_map, Y_map, count, time_count] = ...
    mirrormap_obs_logistic(X_rel,Y_rel,...   % UNUSED -- REMOVE
                           X,Y,...
                           X_map,Y_map,...
                           X_end,Y_end,...
                           i,...
                           sl,sr,sb,st,...  % UNUSED -- REMOVE
                           xmin, xmax, ymin, ymax,...
                           obs_vec,...
                           v,...
                           time_count,dt,...
                           count_max,...
                           obs_frac)
% 
% MIRRORMAP_OBS_LOGISTIC Constructs a path that avoids obstacles
% 
% Input:
%     X_rel,Y_rel = relative trajectory points
%     X,Y         = trajectory points
%     X_map,Y_map = mapped trajectory points
%     X_end,Y_end = midpoint coordinates
%     i           = incrementing index
%     sl          = left hand boundary of the obstacle
%     sr          = right hand boundary of the obstacle
%     sb          = lower boundary of the obstacle
%     st          = upper boundary of the obstacle
%     xmin        = left map boundary
%     xmax        = right map boundary
%     ymin        = bottom map boundary
%     ymax        = top map boundary
%     obs_vec     = vector of obstacle coordinates
%     v           = robot's velocity
%     time_count  = coverage time
%     dt          = time step
%     count_max   = maximum number of times, the robot is allowed to avoid 
%                   obstacles
% 
% Output:
%     X_map,Y_map = mapped trajectory points
%     count       = number of times, the robot avoided the obstacle
%     time_count  = coverage time
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edit:
%     09/14/2021 -- Refactored code and comments.

% Avoid parameter
% obs_frac = 1.;  % Matches with mirrormap_obs usage outside of function.

% Relative distance tolerance for X/Y_rel spacing
dtolerance = 3*v*dt;

%generates the relative trajectory points, only shorter than previous time
X_rel = linspace((X_map(i)-X(i)),(X_end-X(end)),length(X)-i+1);
Y_rel = linspace((Y_map(i)-Y(i)),(Y_end-Y(end)),length(Y)-i+1);
%sets the number of times, the robot avoids the obstacle to 1 as the robot encountered an obstacle once
count = 1;
%loops to count the number of times the robot avoided the obstacle
while count < count_max
    j = 1; %index for the relative trajectory points
    %generates the mapped trajectory points
    for i = i:length(X)
        X_map(i) = X(i) + X_rel(j);
        Y_map(i) = Y(i) + Y_rel(j);
        j = j+1; %increments variable j
        %checks whether the robot's path overlaps the obstacle
        [Xt, Yt] = mirrormap_obs(X_map(i),Y_map(i),obs_vec,xmin,xmax,ymin,ymax,obs_frac);
        if X_map(i) ~= Xt || Y_map(i) ~= Yt % hits obstacle again
            %translates the point outside the obstacle
            X_map(i) = Xt; Y_map(i) = Yt;
            %removes entries of the relative trajectory point
            X_rel = []; Y_rel = [];
            %regenerates the relative trajectory point
            X_rel = linspace((X_map(i)-X(i)),(X_end-X(end)),length(X)-i+1);
            Y_rel = linspace((Y_map(i)-Y(i)),(Y_end-Y(end)),length(Y)-i+1);
            
            % Check that relative spacing is reasonable, terminate if
            % unreasonable. -- PM
            if length(X_rel) > 2
                flag_distance = ...
                    norm([X_rel(1) - X_rel(2);...
                          Y_rel(1) - Y_rel(2)]) > dtolerance;
            else
                flag_distance = ...
                    norm([X_end - Xt; Y_end - Yt]) > dtolerance;
            end
            
            if flag_distance
                count = count_max + 1;
                time_count = time_count + dt;
                if i < length(X_map)
                    X_map(i+1:end) = [];
                    Y_map(i+1:end) = [];
                    X_rel = []; Y_rel = [];
                end
                return
            end
            
            %increments the count as the robot encounters an obstacle
            count = count + 1;
            time_count = time_count + dt;
            break; %breaks to generate the next mapped trajectory point
        else
            %in this case, the mapped point does not lie inside the
            %obstacle and increments the coverage time
            time_count = time_count + dt;
        end
    end
    %checks if the incrementing index equals the length of the trajectory point. What
    %this means, the robot either has reached the desired zone (count <
    %count_max, i == length(X)) or the robot struggled to reach the desired
    %zone (count == count_max, i < length(X))
    if i == length(X)
        break %breaks the while loop
    end
end