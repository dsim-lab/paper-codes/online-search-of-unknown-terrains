function [x,X,Y] = LogisticEquation(r,v,x,X,Y,xmin,xmax,ymin,ymax,time_taken,dt)
%
% LogisticEquation: Dynamic and robotic coordinate points using Logistic
%                   system.
%
% Description: Returns a new, discretized dynamic and robot coordinate
%              position for the robot using the Logistic map system.
% 
% Inputs:
%     r          = chaotic parameter
%     v          = robot's velocity
%     x          = dynamic coordinate
%     X, Y       = robot coordinates
%     xmin       = left side of boundary
%     xmax       = right side of boundary
%     ymin       = lower side of boundary
%     ymax       = upper side of boundary
%     time_taken = time vector
%     dt         = time step
% 
% Outputs:
%     x          = dynamic coordinate
%     X, Y       = robot coordinates
%     time_count = coverage time
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edits:
%     10/06/2021: Reformatted code and comments (PM)

%The X coordinate increases at every iteration. This would become a
%problem as the robot gets stuck at the boundaries. Therefore,
%when X reaches at the right end, theta = pi*x + pi/2 so that the X
%coordinate decreases at every iteration and goes back to the left boundary. 
%Likewise, when X reaches at the left boundary, theta = pi*x - pi/2 
%so that X coordinate increasesat every iteration and goes to the right boundary
   
    if X(end) >= (xmax + xmin)/2 || X(end) >= xmax - 0.01
       for n = length(X):length(X)+length(time_taken)-1 %loops to generate robot coordinates
           theta = pi()*x(n) + pi()/2; %robot will travel to the left hand side of the map
           %Obtains new dynamic and robot entries
           x(n+1) = r*x(n)*(1-x(n));
           X(n+1) = X(n) + dt*v*cos(theta);
           Y(n+1) = Y(n) + dt*v*sin(theta);
           %mirrormap is used to transform outside points into the map
           [X(end),Y(end)] = mirrormap(X(end),Y(end),xmin,xmax,ymin,ymax);
        end
        
    elseif X(end) <= (xmax + xmin)/2 || X(end) <= xmin + 0.01
        for n = length(X):length(X)+length(time_taken)-1 %loops to generate robot coordinates
            theta = pi()*x(n) - pi()/2; %robot will travel to the right hand side of the map
            %Obtains new dynamic and robot entries
            x(n+1) = r*x(n)*(1-x(n));
            X(n+1) = X(n) + dt*v*cos(theta);
            Y(n+1) = Y(n) + dt*v*sin(theta);
            %mirrormap is used to transform outside points into the map
            [X(end),Y(end)] = mirrormap(X(end),Y(end),xmin,xmax,ymin,ymax);
       end
    end
end
